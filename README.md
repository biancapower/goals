# Goals

A simple setup to help focus me on my goals

## today.md

A file in which I add my goals, along with datestamps. This is launched when running "setgoal" (see [aliases](## aliases)).

Timestamp added in vim by using the command `:r !date`.

## aliases

The following aliases have been added to my .zshrc:

```
alias goal="tail -1 ~/goals/today.md"
alias setgoal="vim ~/goals/today.md"
alias allgoals="cat ~/goals/today.md"
alias savegoals="cd ~/goals && git commit -am 'autosave goals' && git push && cd"
```
